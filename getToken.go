package main

import (
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func getToken() (Item, error){
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-2")},
	)
	if err != nil {
		log.Fatalf("Error starting a new session %s", err)
	}

	svc := dynamodb.New(sess)

	tableName := "QNXTWebServiceToken"
	tokenID := "1" //QNXTWebServiceToken has only one item (tokenID=1), updated at regular intervals

	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"EncSTSValID": {
				S: aws.String(tokenID),
			},
		},
	})
	if err != nil {
		log.Println("Got error calling GetItem: ", err)
	}

	if result.Item == nil {
		log.Println("Could not find item ", err)
	}

	item := Item{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		log.Println("Failed to unmarshal Record ", err)
	}

	return item, nil
}
