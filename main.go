package main

import (
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type Item struct {
    EncSTSValID string
	EncSTSVal string
}

type Claim struct {
	ClaimID string
}

type Response struct {
	ClaimID string
	Status string
}

const (
	notPresentError int = iota //notPresentError = 0
	noMemberPresent
)

func returnOutput(errorEnum int) (int, string) {
	switch errorEnum {
	case 0:
			return http.StatusBadRequest, "{\n\tstatus : \"error\"\n\terror_message: \"claim could not be found within the database\"\n}"
	case 1:
			return http.StatusBadRequest, "{\n\tstatus : \"error\"\n\terror_message: \"claim does not have member\"\n}"
	}
	

	return http.StatusBadRequest, "{\n\tstatus : \"error\"\n\terror_message: \"Unknown error occurred\"\n}"
}

func findErrMsg(errMsg string) (int, string) {
	errEnum := -1
	if strings.Contains(errMsg, "claim does not exist in the database") {
		errEnum = notPresentError
	} else if strings.Contains(errMsg, "Claim does not have Member") {
		errEnum = noMemberPresent
	}

	errCode, errBody := returnOutput(errEnum)

	return errCode, errBody
}

func adjudicate(claimid string) (int, string) {
	server := os.Getenv("ADJUDICATION_SERVER")
	env_id := os.Getenv("ENVIRONMENT_ID")
	url := server + "/QNXTAscServices/api/Adjudication/adjudicate?claimId=" + claimid

	token, err := getToken()
	// Create a Bearer string by appending string access token
	var bearer = "Bearer " + token.EncSTSVal

	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		log.Println(err)
	}
	//log.Println("req ---- ", req)
	
	// add authorization & environment id header to the req
	req.Header.Add("Authorization", bearer)
	req.Header.Add("X-Tz-Envid", env_id)

	// Send req using http Client
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}
	//log.Println("resp -------- ", resp)
	defer resp.Body.Close()
	
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}
	//log.Println("body ------ ", body)
	//log.Println(body)
	log.Println("QNXT Adjudicate API Response : " + string([]byte(body)))
	//error_msg := new(ErrorMsg)
	//err = json.Unmarshal([]byte(body), error_msg)
	//log.Println("error message ----- ", error_msg.Error)

	respBody := string([]byte(body))
	respStruct := new(Response)
	_ = json.Unmarshal([]byte(body), respStruct)
	if respStruct.Status == "ADJUCATED" {
		
		return http.StatusOK, "{\n\tstatus: \"adjudicated\"\n\tclaim_id : \"" + respStruct.ClaimID + "\"\n}"
	}
	errCode, errBody := findErrMsg(respBody)
	return errCode, errBody
}

func handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	id := new(Claim)
	
	if req.Body == "" {
		return events.APIGatewayProxyResponse {
			StatusCode: http.StatusBadRequest,
			Body: "{\n\tstatus : \"error\"\n\terror_message: \"input fields cannot be empty\"\n}",
		}, nil
	}

    err := json.Unmarshal([]byte(req.Body), id)
	log.Println("Input Claim ID : " + id.ClaimID)
    if err != nil {
		return events.APIGatewayProxyResponse {
			StatusCode: http.StatusBadRequest,
			Body: "{\n\tstatus : \"error\"\n\terror_message: \"input field does not exist\"\n}",
		}, nil
	}

	respCode, respBody := adjudicate(id.ClaimID)

	return events.APIGatewayProxyResponse{
		StatusCode: respCode,
		Body: respBody,
	}, nil
}

func main() {
	lambda.Start(handler)
}
